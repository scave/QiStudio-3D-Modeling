package com.qiplat.modeling3d.project;

import android.graphics.drawable.Drawable;

import com.qiplat.api.project.BaseProjectManager;
import com.qiplat.api.project.Project;

import java.io.File;

public class Modeling3DProjectManager extends BaseProjectManager {
    @Override
    public String getType() {
        return null;
    }

    @Override
    public int getKind() {
        return 0;
    }

    @Override
    public Drawable getTypeIcon() {
        return null;
    }

    @Override
    public Project newProject(File projectDir) {
        return null;
    }

    @Override
    public boolean isProjectDir(File dir) {
        return false;
    }

    @Override
    public String getIdentifier() {
        return null;
    }
}
